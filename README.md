# kenzan

Kenzan, code challenge - Employee Service

#############################################
Start up
*Run mvn spring-boot:run command to start spring boot
*Above command will start embeded tomcat using the port 8080
*At the path \employeeRest\src\main\resources\data.sql, you will find the employees that are going to be imported at the start of the application.



############################################
Desing Pattern

DAO

-To separete the service layer from the data, in this way we have a low coupling
between components.

-It is easier to write Junit test cases as the persistence layer logic is separate.

-If we need to change the DB, we would just need to update the DAO objects.

Interceptor

-Used to intercept the HttpRequest before reaching the rest services and log it.


#############################################
Operation

Create - POST   - http://localhost:8080/employees
Update - PUT    - http://localhost:8080/employees
Get by ID - GET - http://localhost:8080/employees/{id}
Get All -  Get  - http://localhost:8080/employees
Delete   - DELETE - http://localhost:8080/employees/{id}

Delete user: kenzan and password: kenzan

################################################
Data Base
H2 - internal memory DB

http://localhost:8080/h2/

user: kenzan
password: kenzan



