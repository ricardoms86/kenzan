package com.amdocs.employeeRest.controller;

import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.amdocs.employeeRest.interceptor.EmployeeInterceptor;
import com.amdocs.employeeRest.model.Employee;
import com.amdocs.employeeRest.model.Status;
import com.amdocs.employeeRest.service.EmployeeService;

@RunWith(SpringRunner.class)
@WebMvcTest({EmployeeInterceptor.class,EmployeeController.class})
public class EmployeeControllerTest {
	

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private EmployeeService employeeService;
	
	
	@Test
	public void getAllEmployees() throws Exception {
		
		List<Employee> employees =  new ArrayList<Employee>();
		employees.add(new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE));
		employees.add(new Employee(2,"Sergio","Meza","Sanchez",
				LocalDate.of(1990, 05, 20),LocalDate.of(2016, 02, 20),Status.ACTIVE));		
		when(employeeService.getAllEmployees()).thenReturn(employees);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/employees").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expected = "[{\"id\":1,\"firstName\":\"Ricardo\",\"middleInitial\":\"Meza\",\"lastName\":\"Sanchez\",\"dateOfBirth\":\"1986-02-20\",\"dateOfEmployment\":\"2015-02-20\",\"status\":\"ACTIVE\"},{\"id\":2,\"firstName\":\"Sergio\",\"middleInitial\":\"Meza\",\"lastName\":\"Sanchez\",\"dateOfBirth\":\"1990-05-20\",\"dateOfEmployment\":\"2016-02-20\",\"status\":\"ACTIVE\"}]";
		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}

	@Test
	public void getEmployeeById() throws Exception {
		Employee employee = new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE);
		when(employeeService.getEmployeeById(1)).thenReturn(Optional.of(employee));
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				"/employees/1").accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		System.out.println(result.getResponse());

		String expected = "{\"id\":1,\"firstName\":\"Ricardo\",\"middleInitial\":\"Meza\",\"lastName\":\"Sanchez\",\"dateOfBirth\":\"1986-02-20\",\"dateOfEmployment\":\"2015-02-20\",\"status\":\"ACTIVE\"}";

		JSONAssert.assertEquals(expected, result.getResponse()
				.getContentAsString(), false);
	}
	
	
	@Test
	public void createEmployee() throws Exception {
		
		String expected = "{\"id\":1,\"firstName\":\"Ricardo\",\"middleInitial\":\"Meza\",\"lastName\":\"Sanchez\",\"dateOfBirth\":\"1986-02-20\",\"dateOfEmployment\":\"2015-02-20\",\"status\":\"ACTIVE\"}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post("/employees")
				.accept(MediaType.APPLICATION_JSON).content(expected)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.CREATED.value(), response.getStatus());

	}
	
	
	@Test
	public void updateEmployee() throws Exception {

		Employee employee = new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE);
		when(employeeService.getEmployeeById(1)).thenReturn(Optional.of(employee));
		
		String expected = "{\"id\":1,\"firstName\":\"Ricardo\",\"middleInitial\":\"Meza\",\"lastName\":\"Sanchez\",\"dateOfBirth\":\"1986-02-20\",\"dateOfEmployment\":\"2015-02-20\",\"status\":\"ACTIVE\"}";

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put("/employees")
				.accept(MediaType.APPLICATION_JSON).content(expected)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.OK.value(), response.getStatus());

	}

	
	@Test
	public void deleteEmployeeUnauthorized() throws Exception {

		Employee employee = new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE);
		when(employeeService.getEmployeeById(1)).thenReturn(Optional.of(employee));

		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.delete("/employees/1")
				.accept(MediaType.APPLICATION_JSON)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertEquals(HttpStatus.UNAUTHORIZED.value(), response.getStatus());

	}
}
