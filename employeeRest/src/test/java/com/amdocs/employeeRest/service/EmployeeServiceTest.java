package com.amdocs.employeeRest.service;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.amdocs.employeeRest.dao.EmployeeDao;
import com.amdocs.employeeRest.model.Employee;
import com.amdocs.employeeRest.model.Status;
import com.amdocs.employeeRest.service.EmployeeService;

@RunWith(SpringJUnit4ClassRunner.class)
public class EmployeeServiceTest {

	@Mock
	private EmployeeDao employeeDao;
	
	@InjectMocks
	private EmployeeService employeeService;

	@Test
	public void getAllEmployees() {
		List<Employee> expected =  new ArrayList<Employee>();
		expected.add(new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE));
		expected.add(new Employee(2,"Sergio","Meza","Sanchez",
				LocalDate.of(1990, 05, 20),LocalDate.of(2016, 02, 20),Status.ACTIVE));		
		when(employeeDao.findAll()).thenReturn(expected);
		assertEquals(expected, employeeService.getAllEmployees());
	}
	
	@Test
	public void getEmloyeeById() {
		Employee expected = new Employee(1,"Ricardo","Meza","Sanchez",
				LocalDate.of(1986, 02, 20),LocalDate.of(2015, 02, 20),Status.ACTIVE);
		when(employeeDao.findById(1)).thenReturn(Optional.of(expected));
		assertEquals(expected, employeeService.getEmployeeById(1).get());
	}

	@Test
	public void createEmployee() {
		Employee employee = mock(Employee.class);
		employeeService.create(employee);
		verify(employeeDao).create(employee);
		
	}
	
}
