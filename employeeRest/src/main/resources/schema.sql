create table employee 
(
	id integer not null AUTO_INCREMENT, 
	first_name varchar(255), 
	last_name varchar(255), 
	middle_initial varchar(255),
	date_of_birth date, 
	date_of_employment date, 
	status integer, 
	primary key (id)
);