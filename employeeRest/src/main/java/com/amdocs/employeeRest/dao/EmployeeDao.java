package com.amdocs.employeeRest.dao;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.amdocs.employeeRest.model.Employee;

@Repository
public interface EmployeeDao {

	public void create(Employee employee);
	
	public List<Employee> findAll();
	
	public Optional<Employee>  findById(Integer id);
	
	public void update(Employee employee);
	
	public void delete(Employee employee);
	
}
