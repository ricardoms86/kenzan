package com.amdocs.employeeRest.dao;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.amdocs.employeeRest.model.Employee;
import com.amdocs.employeeRest.model.Status;

@Repository
public class EmployeeDaoImpl implements EmployeeDao{
	
	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void create(Employee employee) {
		employee.setStatus(Status.ACTIVE);	
		entityManager.persist(employee);
	}

	@Override
	public List<Employee> findAll() {
		return entityManager.createQuery("SELECT e FROM Employee e").getResultList();
	}

	@Override
	public Optional<Employee>  findById(Integer id) {
		Employee employee = entityManager.find(Employee.class,id);
		Optional<Employee> optionalEmployee = Optional.ofNullable(employee);
		return optionalEmployee;
	}

	@Override
	public void update(Employee employee) {
		entityManager.merge(employee);
	}

	@Override
	public void delete(Employee employee) {
		employee.setStatus(Status.INACTIVE);
		entityManager.persist(employee);
	}

	
}
