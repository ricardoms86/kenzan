package com.amdocs.employeeRest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.Pbkdf2PasswordEncoder;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter{
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		Pbkdf2PasswordEncoder encoder = new Pbkdf2PasswordEncoder();
		String password = encoder.encode("kenzan");
		auth.inMemoryAuthentication().passwordEncoder(encoder).withUser("kenzan").password(password)
		.roles("ADMIN");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.httpBasic().and().authorizeRequests().antMatchers(HttpMethod.DELETE,"/employees/{id}").hasRole("ADMIN").and()
		.csrf().disable().headers().frameOptions().disable();
	}

}
