package com.amdocs.employeeRest.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.amdocs.employeeRest.exception.EmployeeNotFoundException;
import com.amdocs.employeeRest.model.Employee;
import com.amdocs.employeeRest.model.Status;
import com.amdocs.employeeRest.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@GetMapping("/employees")
	private ResponseEntity<List<Employee>> getAllEmployees() {
        return new ResponseEntity<List<Employee>>(employeeService.getAllEmployees(),
        			HttpStatus.OK);  
    }
	
	@GetMapping("/employees/{id}")
	private ResponseEntity<Employee> getEmployee(@PathVariable("id") Integer id) {
		Optional<Employee> employee = employeeService.getEmployeeById(id);
		employee.filter(empl -> Status.ACTIVE.equals(empl.getStatus()))
		.orElseThrow(() -> new EmployeeNotFoundException("id :"+id));
		return new ResponseEntity<Employee>(employee.get(),
    			HttpStatus.OK);
	}
	
	@PostMapping("/employees")
	private ResponseEntity<String> createEmployee(@Valid @RequestBody Employee employee) {
		employeeService.create(employee);
        return new ResponseEntity<String>("Employee Created Succesfully",HttpStatus.CREATED);
    }

	@PutMapping("/employees")
	private ResponseEntity<String> updateEmployee(@Valid @RequestBody Employee employeeRequest) {
		Optional<Employee> employee = employeeService.getEmployeeById(employeeRequest.getId());
		employee.orElseThrow(() -> new EmployeeNotFoundException("id :"+employeeRequest.getId()));
		employeeService.update(employeeRequest);
		return new ResponseEntity<String>("Employee updated succesfully",HttpStatus.OK);
	}
	
	@DeleteMapping("/employees/{id}")
	private ResponseEntity<String> deleteEmployee(@PathVariable("id") Integer id) {		
		Optional<Employee> employee = employeeService.getEmployeeById(id);
		employee.filter(empl -> Status.ACTIVE.equals(empl.getStatus()))
		.orElseThrow(() -> new EmployeeNotFoundException("id :"+id));
		employeeService.delete(employee.get());
		return new ResponseEntity<String>("Employee Inactive",HttpStatus.OK);
	}



}
