package com.amdocs.employeeRest.model;

public enum Status {
	ACTIVE("ACTIVE",0),
	INACTIVE("INACTIVE",1);
	
	private Integer id;
	private String value;
	
	private Status(String value,Integer id){
		this.id = id;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
