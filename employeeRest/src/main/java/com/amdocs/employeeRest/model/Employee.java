package com.amdocs.employeeRest.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;

@ApiModel(description="Employee")
@Entity
public class Employee {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@NotNull
	@Size(min=2, message="Name should have atleast 2 characters")
	private String firstName;
	
	@NotNull
	@Size(min=2, message="Middle initial should have atleast 2 characters")
	private String middleInitial;
	
	@NotNull
	@Size(min=2, message="Last name should have atleast 2 characters")
	private String lastName;
	
	@NotNull
	@Past(message="Date of Birth should be a past date")
	private LocalDate dateOfBirth;
	
	@NotNull
	@PastOrPresent(message="Date of Employment should be a past date or present")
	private LocalDate dateOfEmployment;
	private Status status;
	
	public Employee() {
		super();
	}
	
	public Employee(Integer id, String firstName, String middleInitial, String lastName, LocalDate dateOfBirth,
			LocalDate dateOfEmployment, Status status) {
		this.id = id;
		this.firstName = firstName;
		this.middleInitial = middleInitial;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.dateOfEmployment = dateOfEmployment;
		this.status = status;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleInitial() {
		return middleInitial;
	}
	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public LocalDate getDateOfEmployment() {
		return dateOfEmployment;
	}
	public void setDateOfEmployment(LocalDate dateOfEmployment) {
		this.dateOfEmployment = dateOfEmployment;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
}
