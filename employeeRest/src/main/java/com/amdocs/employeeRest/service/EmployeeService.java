package com.amdocs.employeeRest.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amdocs.employeeRest.dao.EmployeeDao;
import com.amdocs.employeeRest.model.Employee;
import com.amdocs.employeeRest.model.Status;

@Transactional
@Service
public class EmployeeService {
	
	@Autowired
	private EmployeeDao employeeDao;
	
	public List<Employee> getAllEmployees(){
		List<Employee> employees = new ArrayList<Employee>();
		
		employees = employeeDao.findAll().stream()
				.filter( employee -> Status.ACTIVE.equals(employee.getStatus()))
				.collect(Collectors.toList());
		return employees;
	}
	
	public Optional<Employee> getEmployeeById(Integer id) {
        return employeeDao.findById(id);
    }

	 public void create(Employee employee) 
	 {
		    employeeDao.create(employee);
	 }
	 
    public void update(Employee employee) {
    	employeeDao.update(employee);
    }

    public void delete(Employee employee) {
    	employeeDao.delete(employee);
    }
	

}
