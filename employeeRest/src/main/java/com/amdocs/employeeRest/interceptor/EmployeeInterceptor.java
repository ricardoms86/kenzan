package com.amdocs.employeeRest.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import com.amdocs.employeeRest.controller.EmployeeController;

@Component
public class EmployeeInterceptor extends HandlerInterceptorAdapter {
	
	Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		ContentCachingRequestWrapper requestCacheWrapperObject
	      = new ContentCachingRequestWrapper(request);
		requestCacheWrapperObject.getParameterMap();
		logger.info("Method: "+requestCacheWrapperObject.getMethod()+", URI: "
				+requestCacheWrapperObject.getServletPath());
		
		return super.preHandle(request,response,handler);
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		super.afterCompletion(request, response, handler, ex);
	}

}
