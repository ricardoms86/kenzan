package com.amdocs.employeeRest.error;

import java.time.LocalDate;

import org.springframework.http.HttpStatus;

public class ErrorDetails {
	
	private LocalDate localDate;
	private String message;
    private String details;
    private HttpStatus status;

    public ErrorDetails(LocalDate localDate, String message, String details,HttpStatus status) {
        this.localDate = localDate;
        this.message = message;
        this.details = details;
        this.status = status;
      }
    
    
    public LocalDate getLocalDate() {
		return localDate;
	}
	public void setLocalDate(LocalDate localDate) {
		this.localDate = localDate;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}


	public HttpStatus getStatus() {
		return status;
	}


	public void setStatus(HttpStatus status) {
		this.status = status;
	}

}
