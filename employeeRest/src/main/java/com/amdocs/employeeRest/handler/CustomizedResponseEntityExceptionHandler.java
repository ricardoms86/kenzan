package com.amdocs.employeeRest.handler;

import java.time.LocalDate;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.amdocs.employeeRest.error.ErrorDetails;
import com.amdocs.employeeRest.exception.EmployeeNotFoundException;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
	
	
	 @ExceptionHandler(Exception.class)
	  public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(LocalDate.now(), ex.getMessage(),
	        request.getDescription(false),HttpStatus.INTERNAL_SERVER_ERROR);
	    return new ResponseEntity(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
	  }

	
	@ExceptionHandler(EmployeeNotFoundException.class)
	  public final ResponseEntity<ErrorDetails> handleEmployeeNotFoundException(EmployeeNotFoundException ex, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(LocalDate.now(), ex.getMessage(),
	        request.getDescription(false),HttpStatus.NOT_FOUND);
	    return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
	  }

	
	@Override
	  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
	      HttpHeaders headers, HttpStatus status, WebRequest request) {
	    ErrorDetails errorDetails = new ErrorDetails(LocalDate.now(), "Validation Failed",
	        ex.getBindingResult().getFieldError().getDefaultMessage(), HttpStatus.BAD_REQUEST);
	    return new ResponseEntity(errorDetails, HttpStatus.BAD_REQUEST);
	 } 
}
